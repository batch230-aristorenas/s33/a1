//console.log("Hello World");



fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {

	let enumerate = json.map((todo => {
		return todo.title;
	}))

	console.log(enumerate);
})


fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers:{
		"Content-Type" : "application/json"
	},
	body:JSON.stringify({
		complpeted: false,
		id: 201,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers:{
		"Content-Type" : "application/json"
	},
	body:JSON.stringify({
		title: "Updated To Do List Item",
		description:"To update the my to do list wi th a different data structure",
		status: "Pending",
		dateCompleted: "Pending",
		userId:1
		})
	})
.then(response => response.json())
.then(json => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers:{
		"Content-Type" : "application/json"
	},
	body:JSON.stringify({
		status: "Complete",
		dateCompleted: "08/02/23"
	    })
	})
.then(response => response.json())
.then(json => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})
.then(response => response.json())
.then(json => console.log(json))